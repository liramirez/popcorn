require 'test_helper'

class TipoSalasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_sala = tipo_sala(:one)
  end

  test "should get index" do
    get tipo_salas_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_sala_url
    assert_response :success
  end

  test "should create tipo_sala" do
    assert_difference('TipoSala.count') do
      post tipo_salas_url, params: { tipo_sala: { ts_desc: @tipo_sala.ts_desc } }
    end

    assert_redirected_to tipo_sala_url(TipoSala.last)
  end

  test "should show tipo_sala" do
    get tipo_sala_url(@tipo_sala)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_sala_url(@tipo_sala)
    assert_response :success
  end

  test "should update tipo_sala" do
    patch tipo_sala_url(@tipo_sala), params: { tipo_sala: { ts_desc: @tipo_sala.ts_desc } }
    assert_redirected_to tipo_sala_url(@tipo_sala)
  end

  test "should destroy tipo_sala" do
    assert_difference('TipoSala.count', -1) do
      delete tipo_sala_url(@tipo_sala)
    end

    assert_redirected_to tipo_salas_url
  end
end
