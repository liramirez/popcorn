class CargosController < ApplicationController
  before_action :set_cargo, only: [:show, :edit, :update, :destroy]

  # GET /cargos
  def index
    @cargos = Cargo.all
  end

  # GET /cargos/1
  def show
  end

  # GET /cargos/new
  def new
    @cargo = Cargo.new
  end

  # GET /cargos/1/edit
  def edit
  end

  # POST /cargos
  def create
    @cargo = Cargo.new(cargo_params)

    if @cargo.save
      redirect_to @cargo, notice: 'Cargo ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Cargo no se pudo agregar.'
    end
  end

  # PATCH/PUT /cargos/1
  def update
    if @cargo.update(cargo_params)
      redirect_to @cargo, notice: 'Cargo ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Cargo no se pudo actualizar.'
    end
  end

  # DELETE /cargos/1
  def destroy
    @cargo.destroy
    redirect_to page_mtb_path, notice: 'Cargo ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cargo
      @cargo = Cargo.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cargo_params
      params.require(:cargo).permit(:car_id, :car_desc)
    end
end
