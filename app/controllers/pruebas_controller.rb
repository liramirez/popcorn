class PruebasController < ApplicationController
  before_action :set_prueba, only: [:edit, :update, :destroy]

  # GET /pruebas
  def index
    @pruebas = Prueba.all
  end

  # GET /pruebas/new
  def new
    @prueba = Prueba.new
  end

  # GET /pruebas/1/edit
  def edit
  end

  # POST /pruebas
  def create
    @prueba = Prueba.new(prueba_params)

    if @prueba.save
      redirect_to @prueba, notice: 'Prueba ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /pruebas/1
  def update
    if @prueba.update(prueba_params)
      redirect_to @prueba, notice: 'Prueba ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /pruebas/1
  def destroy
    @prueba.destroy
    redirect_to pruebas_url, notice: 'Prueba was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prueba
      @prueba = Prueba.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def prueba_params
      params.require(:prueba).permit(:titulo, :precio)
    end
end
