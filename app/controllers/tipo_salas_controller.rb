class TipoSalasController < ApplicationController
  before_action :set_tipo_sala, only: [:show, :edit, :update, :destroy]

  # GET /tipo_salas
  def index
    @tipo_salas = TipoSala.all
  end

  # GET /tipo_salas/1
  def show
  end

  # GET /tipo_salas/new
  def new
    @tipo_sala = TipoSala.new
  end

  # GET /tipo_salas/1/edit
  def edit
  end

  # POST /tipo_salas
  def create
    @tipo_sala = TipoSala.new(tipo_sala_params)

    if @tipo_sala.save
      redirect_to @tipo_sala, notice: 'Tipo sala ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Tipo sala no se pudo agregar.'
    end
  end

  # PATCH/PUT /tipo_salas/1
  def update
    if @tipo_sala.update(tipo_sala_params)
      redirect_to @tipo_sala, notice: 'Tipo sala ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Tipo sala no se pudo agregar.'
    end
  end

  # DELETE /tipo_salas/1
  def destroy
    @tipo_sala.destroy
    redirect_to page_mtb_path, notice: 'Tipo sala ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_sala
      @tipo_sala = TipoSala.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_sala_params
      params.require(:tipo_sala).permit(:ts_id, :ts_desc)
    end
end
