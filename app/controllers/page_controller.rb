class PageController < ApplicationController

  def index
    @trab = Trabajador.count
    @tick = Ticket.count
    @fun = Funcion.count
    @pel = Pelicula.count
  end

  def vista
    @vis1 = VTickPeli.all
    @vis2 = VPeliCanHor.all
  end

  def procedimiento
    plsql.PRO_RESUMEN_TICKET
    @esta = ResumenTicket.all
  end

  def auditor
    @audi = Auditor.all
  end

  def mtb
    @tablas = ["Cargo","Tipo Sala","Tipo Ticket","Estado Funcion", "Estado Ticket", "Horario", "Edad", "Genero", "Pais Origen"]
    @cargos = Cargo.all
    @cargo = Cargo.new
    @tipo_salas = TipoSala.all
    @tipo_sala = TipoSala.new
    @tipo_tickets = TipoTicket.all
    @tipo_ticket = TipoTicket.new
    @est_funs = EstFun.all
    @est_fun = EstFun.new
    @est_ticks = EstTick.all
    @est_tick = EstTick.new
    @horarios = Horario.all
    @horario = Horario.new
    @edads = Edad.all
    @edad = Edad.new
    @generos = Genero.all
    @genero = Genero.new
    @pais_origens = PaisOrigen.all
    @pais_origen = PaisOrigen.new
  end

end
