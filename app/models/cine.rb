class Cine < ApplicationRecord
  has_many :trabajador, :foreign_key => :tra_cine, :primary_key => :cine_id
  has_many :sala, :foreign_key => :sala_cine, :primary_key => :cine_id

  validates :cine_id , uniqueness: true
end
