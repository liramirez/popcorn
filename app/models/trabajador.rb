class Trabajador < ApplicationRecord
  belongs_to :cargo, :foreign_key => :tra_car, :primary_key => :car_id
  belongs_to :cine, :foreign_key => :tra_cine, :primary_key => :cine_id

  has_one :admin, :foreign_key => :admin_id, :primary_key => :trabajador_rut

  validates :tra_id , uniqueness: true
end
