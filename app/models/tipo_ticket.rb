class TipoTicket < ApplicationRecord
  has_many :ticket, :foreign_key => :tick_tt, :primary_key => :tt_id

  validates :tt_id , uniqueness: true
end
