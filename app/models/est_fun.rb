class EstFun < ApplicationRecord
  has_many :funcion, :foreign_key => :fun_ef, :primary_key => :ef_id

  validates :ef_id , uniqueness: true
end
