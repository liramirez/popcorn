class Funcion < ApplicationRecord
  belongs_to :sala, :foreign_key => :fun_sala, :primary_key => :sala_id
  belongs_to :est_fun, :foreign_key => :fun_ef, :primary_key => :ef_id
  belongs_to :pelicula, :foreign_key => :fun_peli, :primary_key => :peli_id
  belongs_to :horario, :foreign_key => :fun_hor, :primary_key => :hor_id
  has_many :ticket, :foreign_key => :tick_fun, :primary_key => :fun_id

  validates :fun_id , uniqueness: true
end
